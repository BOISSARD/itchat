iTchat
======

Installation :
--------------

Clonez le repository :
```
git clone https://gitlab.com/BOISSARD/itchat.git
```

Dirigez vous dans le dossier puis installez les paquets nécéssaires à partir de [cocoapods](https://cocoapods.org/) :
```
pod install
```

**Attention** peut-être que l'adresse IP/nom de domaine du serveur n'est pas bon, pour cela vérifiez dans le fichier `itchat/Api-Managers/TchatApiManager.swift`, que l'url du socket soit (pour une connexion à mon serveur personnel) :
```swift
self.manager = SocketManager(socketURL: URL(string: "http://tchat-api.boissard.info")!)
// self.manager = SocketManager(socketURL: URL(string: "http://192.168.56.1:20381")!)
```

Utilisation :
-------------

Pour utiliser l'application, lors de l'étape de connexion vous pouvez utiliser les identifiants :
*  Pseudo : "**Invite**"
*  Mot de passe : "**invite**"

Objectifs :
-----------

iTchat est un projet développé avec [swift 4](https://www.apple.com/fr/swift/) afin de réaliser une application pour iOS sur iPhone.

C'est un projet réalisé en 4ème année d'étudiant ingénieur informatique à l'ESIREM de Dijon.

Cette application a pour but une messagerie instantané.  
Cette application se veut relativement simple en fonctionnalité mais efficace dans le principe.


Fonctionnement :
----------------

Pour fonctionner, cette application se connecte à partir de socket sur une api héberger sur mon serveur personnel, cette api est développée en NodeJs.

Pour communiquer avec l'api l'application swift utilise le paquet cocoapods [Socket.IO-Client-Swift](https://github.com/socketio/socket.io-client-swift) développé justement dnas le but de communiquer avec une api NodeJS de la même manière que le paquet [Socket.io-client](https://www.npmjs.com/package/socket.io-client) de NodeJS.

Rendu :
-------

Voici la page de connexion :                             |  Et le salon de conversation
:-------------------------------------------------------:|:-------------------------:
![Alt text](/screenshots/login.png?raw=true "Connxion")  |  ![Alt text](/screenshots/tchat.png?raw=true "Conversation")
