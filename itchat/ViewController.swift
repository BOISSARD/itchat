//
//  ViewController.swift
//  itchat
//
//  Created by Clément on 18/12/2018.
//  Copyright © 2018 boissard.info. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var loadingLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //TchatApiManager.sharedInstance.establishConnection()
        loadingLabel.text = "En attente de connexion..."
        //self.displayLoginViewCrontroller()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.displayLoginViewCrontroller()
    }

    private func displayLoginViewCrontroller() {
        let storyboard = UIStoryboard(name: "LoginView", bundle: nil)
        let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginViewControllerIdentifier")
        self.present(loginVC, animated: true, completion: nil)
    }
    
}

extension UIView {
    
    func addConstraintsWithFormat(format: String, views: UIView...){
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
    
}
