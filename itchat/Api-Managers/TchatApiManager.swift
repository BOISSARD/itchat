//
//  TchatApiManager.swift
//  itchat
//
//  Created by Clément on 06/01/2019.
//  Copyright © 2019 boissard.info. All rights reserved.
//

import Foundation
import SocketIO

class TchatApiManager : NSObject {
    
    static let instance = TchatApiManager()
    var manager: SocketManager
    var socket: SocketIOClient
    static var decoder : JSONDecoder = JSONDecoder()

    override init() {
        self.manager = SocketManager(socketURL: URL(string: "http://tchat-api.boissard.info:80/")!)
        //self.manager = SocketManager(socketURL: URL(string: "http://192.168.56.1:20381")!)
        self.socket = manager.defaultSocket
        super.init()
    }
    
}
