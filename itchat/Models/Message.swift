//
//  Message.swift
//  itchat
//
//  Created by Clément on 06/01/2019.
//  Copyright © 2019 boissard.info. All rights reserved.
//

import Foundation

class Message : CustomStringConvertible{
    
    var content: String
    var date: Date?
    var user: User?
    
    init(content: String, date: Date?, user: User?) {
        self.content = content
        self.date = date
        self.user = user
    }
    
    convenience init(from json: NSDictionary) {
        let content:String = json["content"]! as! String
        let dateStr:String = json["date"]! as! String
        let date : Date? = jsonDateToSwiftDate(jsonDate: dateStr)
        self.init(content: content, date: date, user: nil)
    }
    
    var description: String {
        return "\(content) \(swiftDateToJsonDate(date: date!)) \(user?.pseudo ?? "unknown user")"
    }
    
    public static func ==(of: Message, with: Message) -> Bool {
        if(of.user == nil && with.user == nil) {
            return of.content == with.content && of.date == with.date
        } else {
            return of.content == with.content && of.date == with.date && of.user! == with.user!
        }
    }
}

func jsonDateToSwiftDate(jsonDate: String) -> Date? {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    if let date = formatter.date(from: jsonDate) {
        return date
    } else {
        return nil
    }
}

func swiftDateToJsonDate(date: Date?) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    if date != nil {
        return formatter.string(from: date!)
    } else {
        return ""
    }
}
