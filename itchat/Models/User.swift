//
//  User.swift
//  itchat
//
//  Created by Clément on 06/01/2019.
//  Copyright © 2019 boissard.info. All rights reserved.
//

import Foundation
import SwiftHash

class User : CustomStringConvertible {
    
    var id :String
    var pseudo :String
    var mail :String
    var password :String
    var avatar :String
    var color :String
    
    init(pseudo: String, password: String) {
        self.id = ""
        self.pseudo = pseudo
        self.password = password
        self.mail = ""
        self.avatar = ""
        self.color = "#000000"
    }
    
    convenience init(pseudo: String, mail: String, password: String, avatar: String, color: String){
        self.init(pseudo: pseudo, password: password)
        self.id = MD5(mail).uppercased()
        self.mail = mail
        self.avatar = avatar
        self.color = color
    }
    
    convenience init(from json: NSDictionary) {
        let pseudo:String = json["pseudo"]! as! String
        let mail:String = json["mail"]! as! String
        let password:String = json["passwd"]! as! String
        let avatar:String = json["avatar"]! as! String
        let color:String = json["color"]! as! String
        self.init(pseudo: pseudo, mail: mail, password: password, avatar: avatar, color: color)
    }
    
    public var description: String {
        return "\(self.pseudo) \(self.mail) \(self.color)"
    }
    
    public static func == (of: User, with: User) -> Bool {
        return of.id == with.id
    }

}
