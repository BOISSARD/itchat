//
//  Manager.swift
//  itchat
//
//  Created by Clément on 06/01/2019.
//  Copyright © 2019 boissard.info. All rights reserved.
//

import Foundation
import SocketIO

class Manager {
    
    var users : [User]
    var connecteds : [User]
    var messages : [Message]
    var current: User?
    var connected : Bool
    var signedIn : Bool
    
    var tchatapi: TchatApiManager = TchatApiManager.instance
    
    init() {
        self.users = []
        self.connecteds = []
        self.messages = []
        self.connected = false
        self.signedIn = false
    }
    
    var alert: UIAlertController?
    
    func establishConnection(){
        self.tchatapi.socket.connect()
        self.tchatapi.socket.on("connect") { (data, ack) -> Void in
            print("Connected")
            self.connected = true
            if(self.current != nil){
                self.tchatapi.socket.emit("sign in", ["pseudo": self.current!.pseudo, "passwd": self.current!.password])
            }
            self.getData()
        }
        self.tchatapi.socket.on("disconnect") { (data, ack) -> Void in
            self.connected = false
            print("Disconnected")
        }
    }
    
    func closeConnection(){
        self.tchatapi.socket.disconnect()
        self.connected = false
    }
    
    func ajouterUser(data: NSDictionary){
        let user = User(from: data)
        if !self.users.contains(where: { (u) -> Bool in
            return u == user
        }) {
            self.users.append(user)
        }
    }
    
    func ajouterConnecteds(data: NSDictionary){
        let user = User(from: data)
        if !self.connecteds.contains(where: { (u) -> Bool in
            return u == user
        }) {
            self.connecteds.append(user)
        }
    }
    
    func ajouterMessage(message: Message){
        if !self.messages.contains(where: { $0 == message }) {
            self.messages.append(message)
        }
        
    }
    
    func getData(){
        print("getting data...")
        
        self.tchatapi.socket.emit("users")
        self.tchatapi.socket.emit("connecteds")
        self.tchatapi.socket.emit("messages")
        
        self.tchatapi.socket.on("users"){ data, _ in
            if let datas = data[0] as? NSArray {
                for json in datas {
                    if let data = json as? NSDictionary {
                        self.ajouterUser(data: data)
                    }
                }
            }
        }
        
        self.tchatapi.socket.on("connecteds"){ data, _ in
            if let datas = data[0] as? NSArray {
                for json in datas {
                    if let data = json as? NSDictionary {
                        self.ajouterConnecteds(data: data)
                    }
                }
            }
        }
        
        self.tchatapi.socket.on("messages"){ data, _ in
            if let datas = data[0] as? NSArray {
                for json in datas {
                    if let data = json as? NSDictionary {
                        let message = Message(from: data)
                        var pos = -1
                        for (index, user) in self.users.enumerated() {
                            if(user.id == (data["user"] as! String).uppercased()) {
                                pos = index
                                break
                            }
                        }
                        if(pos >= 0) {
                            message.user = self.users[pos]
                        }
                        self.ajouterMessage(message: message)
                    }
                }
            }
        }
    }
    
}
