//
//  AlertViewController.swift
//  itchat
//
//  Created by Clément on 11/01/2019.
//  Copyright © 2019 boissard.info. All rights reserved.
//

import UIKit

extension UIAlertController {
    
    func defineType(type: AlertType, handler: ((UIAlertAction) -> Void)? = nil){
        var color: UIColor
        var boutonOk = true
        switch type {
        case .loading :
            if self.title == nil {
                self.title = "Chargement"
            }
            color = UIColor.black
            boutonOk = false
            let indicator = UIActivityIndicatorView(frame: CGRect(x: 20, y: 5, width: 50, height: 50))
            indicator.hidesWhenStopped = true
            indicator.style = UIActivityIndicatorView.Style.gray
            indicator.startAnimating()
            self.view.addSubview(indicator)
        case .info :
            if self.title == nil {
                    self.title = "Informations"
            }
            color = UIColor.blue
        case .error :
            if self.title == nil {
                self.title = "Erreur"
            }
            color = UIColor.red
            /*let symbol = UITextView(frame: CGRect(x: 40, y: 20, width: 20, height: 20))
            symbol.text = "⚠"
            symbol.tintColor = self.view.tintColor
            self.view.addSubview(symbol)*/
        case .success :
            if self.title == nil {
                self.title = "Réussi"
            }
            color = UIColor.green
        case .normal :
            color = UIColor.black
        }
        self.view.tintColor = color
        if(boutonOk){
            let actionOk = UIAlertAction(title: "OK", style: .cancel, handler: handler)
            self.addAction(actionOk)
        }
        let title = NSMutableAttributedString(string: self.title!)
        title.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: NSMakeRange(0, NSString(string: title.string).length))
        self.setValue(title, forKey: "attributedTitle")
    }
    
    enum AlertType{
        case normal
        case loading
        case info
        case error
        case success
    }
    
}
