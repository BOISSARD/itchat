//
//  LoginViewController.swift
//  itchat
//
//  Created by Clément on 05/01/2019.
//  Copyright © 2019 boissard.info. All rights reserved.
//

import UIKit

class LoginViewController : UIViewController {
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var manager : Manager = (UIApplication.shared.delegate as! AppDelegate).manager
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onConnexionTap(_ sender: UIButton) {
        guard let pseudo = usernameTextField.text else {
            return
        }
        guard let password = passwordTextField.text else {
            return
        }
        
        let load = UIAlertController(title: "Connexion...", message: "En attente d'une réponse du serveur", preferredStyle: .alert)
        load.defineType(type: .loading)
        self.present(load, animated: true, completion: nil)
        self.manager.tchatapi.socket.emit("sign in", ["pseudo": pseudo, "passwd": password])
        
        self.manager.tchatapi.socket.on("sign in") { data, _ in
            
            if let json = data[0] as? NSDictionary {
                if(json["error"] == nil){
                    self.manager.current = User(from: json)
                    load.dismiss(animated: true, completion: {
                        let storyboard = UIStoryboard(name: "TchatView", bundle: nil)
                        let tchatVC = storyboard.instantiateViewController(withIdentifier: "TchatViewControllerIdentifier")
                        self.present(tchatVC, animated: true, completion: nil)
                    })
                }
                else {
                    var message: String
                    if let jsonErr = json["error"] as? NSDictionary {
                        message = jsonErr["message"] as! String
                    } else {
                        message = "Erreur !"
                    }
                    load.dismiss(animated: true, completion: {
                        let error = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                        error.defineType(type: .error)
                        self.present(error, animated: true, completion: nil)
                    })
                }
            }
        }
        
    }
}
