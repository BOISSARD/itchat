//
//  TchatViewController.swift
//  itchat
//
//  Created by Clément on 11/01/2019.
//  Copyright © 2019 boissard.info. All rights reserved.
//

import UIKit

let messageTextFontSize: CGFloat = 16

class TchatViewController : UICollectionViewController, UICollectionViewDelegateFlowLayout {

    var manager : Manager = (UIApplication.shared.delegate as! AppDelegate).manager
    
    private let cellId = "cellId"
    private var width: CGFloat = 0.0
    private var porcent: CGFloat = 0.7
    private var collectionViewLength: CGFloat = 8
    
    let bottomContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear//.init(white: 0, alpha: 0.3)
        view.layer.cornerRadius = 15
        view.layer.masksToBounds = true
        return view
    }()
    
    let messageInputContainerView: UIView = {
       let view = UIView()
        view.backgroundColor = UIColor.init(white: 0, alpha: 0.3)
        view.layer.cornerRadius = 15
        view.layer.masksToBounds = true
        return view
    }()
    
    let inputTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Message..."
        textField.textColor = UIColor.white
        return textField
    }()
    
    let profilImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 15
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    lazy var sendButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("✉️", for: .normal)
        button.backgroundColor = UIColor.init(white: 0, alpha: 0.3)
        button.layer.cornerRadius = 15
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(handleSend), for: .touchUpInside)
        return button
    }()
    
    var bottomConstraint: NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        width = view.frame.width
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        /*layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0*/
        
        collectionView?.collectionViewLayout = layout
        collectionView?.register(TchatMessageCell.self, forCellWithReuseIdentifier: cellId)
        
        view.addSubview(bottomContainerView)
        view.addConstraintsWithFormat(format: "H:|-4-[v0]-4-|", views: bottomContainerView)
        view.addConstraintsWithFormat(format: "V:[v0(38)]", views: bottomContainerView)
        
        bottomConstraint = NSLayoutConstraint(item: bottomContainerView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: -12)
        view.addConstraint(bottomConstraint!)
        
        self.setupBottomContainer()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keboardNotificated), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keboardNotificated), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        //self.collectionView?.scrollToItem(at: IndexPath(item: self.manager.messages.count - 1, section: 0), at: , animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        scrollBottom()
        
        var loadAlert: UIAlertController?
        
        self.manager.tchatapi.socket.on("message"){ data, _ in
            if let json = data[0] as? NSDictionary {
                if let datas = json["message"] as? NSDictionary {
                    let message = Message(from: datas)
                    var pos = -1
                    for (index, user) in self.manager.users.enumerated() {
                        if(user.id == (datas["user"] as! String).uppercased()) {
                            pos = index
                            break
                        }
                    }
                    if(pos >= 0) {
                        message.user = self.manager.users[pos]
                    }
                    self.manager.ajouterMessage(message: message)
                    self.collectionView.insertItems(at: [IndexPath(item: self.manager.messages.count - 1, section: 0)])
                    self.scrollBottom()
                }
            }
        }
        
        self.manager.tchatapi.socket.on("connect") { (data, ack) -> Void in
            self.manager.connected = true
            loadAlert?.dismiss(animated: false, completion: {
                if(self.manager.current != nil){
                    self.manager.tchatapi.socket.emit("sign in", ["pseudo": self.manager.current!.pseudo, "passwd": self.manager.current!.password])
                } else {
                    self.dismiss(animated: true, completion: nil)
                }
                self.manager.getData()
            })
        }
        
        self.manager.tchatapi.socket.on("disconnect") { (data, ack) -> Void in
            print("Disconnected here")
            self.manager.connected = false
            loadAlert = UIAlertController(title: "Connexion...", message: "En attente de connexion au serveur", preferredStyle: .alert)
            loadAlert!.defineType(type: .loading)
            self.present(loadAlert!, animated: true, completion: nil)
        }
    }
    
    private func setupBottomContainer(){
        bottomContainerView.addSubview(profilImageView)
        bottomContainerView.addConstraintsWithFormat(format: "H:|-6-[v0(30)]", views: profilImageView)
        bottomContainerView.addConstraintsWithFormat(format: "V:[v0(30)]-4-|", views: profilImageView)
        self.profilImageView.backgroundColor = UIColor.init(hexa: manager.current!.color + "64")
        setImageProfilFromURL(url: manager.current!.avatar, imgView: self.profilImageView)
        
        bottomContainerView.addSubview(messageInputContainerView)
        bottomContainerView.addConstraintsWithFormat(format: "H:|-46-[v0]-46-|", views: messageInputContainerView)
        bottomContainerView.addConstraintsWithFormat(format: "V:|[v0]|", views: messageInputContainerView)
        
        messageInputContainerView.addSubview(inputTextField)
        messageInputContainerView.addConstraintsWithFormat(format: "H:|-15-[v0]-15-|", views: inputTextField)
        messageInputContainerView.addConstraintsWithFormat(format: "V:|[v0]|", views: inputTextField)
        
        bottomContainerView.addSubview(sendButton)
        bottomContainerView.addConstraintsWithFormat(format: "H:[v0]-6-|", views: sendButton)
        bottomContainerView.addConstraintsWithFormat(format: "V:[v0(30)]-4-|", views: sendButton)
        
    }
    
    @objc private func keboardNotificated(notification: Notification){
        if let userInfo = notification.userInfo {
            let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
            
            if isKeyboardShowing {
                bottomConstraint?.constant = -(keyboardFrame?.height)!
                messageInputContainerView.backgroundColor = UIColor.init(white: 0.3, alpha: 0.99)
            } else {
                bottomConstraint?.constant = -12
                messageInputContainerView.backgroundColor = UIColor.init(white: 0, alpha: 0.3)
            }
            
            UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
                    self.view.layoutIfNeeded()
                }, completion: { (completed) in
                    if isKeyboardShowing {
                        self.scrollBottom()
                        //self.collectionView?.scrollToItem(at: IndexPath(item: self.manager.messages.count - 1, section: 0), at: .top, animated: true)
                    }
                })
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        inputTextField.endEditing(true)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return manager.messages.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! TchatMessageCell
        
        cell.messageTextView.text = manager.messages[indexPath.item].content
        
        let messageText = manager.messages[indexPath.item].content
        let estimatedFrame = NSString(string: messageText).boundingRect(with: CGSize(width: width * porcent , height: 1000000), options: NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin), attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: messageTextFontSize)], context: nil)
        
        if manager.messages[indexPath.item].user! == manager.current! {
            cell.setMessageTextView(isSender: true)
            cell.messageTextView.frame = CGRect(x: width - estimatedFrame.width - 32, y: 0, width: estimatedFrame.width + 16, height: estimatedFrame.height + 18)
            cell.textBubbleView.frame = CGRect(x: width - estimatedFrame.width - 40, y: 0, width: estimatedFrame.width + 30, height: estimatedFrame.height + 18)
            cell.profilImageView.isHidden = true
        } else {
            cell.setMessageTextView(isSender: false)
            setImageProfilFromURL(url: manager.messages[indexPath.item].user!.avatar, imgView: cell.profilImageView)
            cell.profilImageView.backgroundColor = UIColor.init(hexa: manager.messages[indexPath.item].user!.color + "1E")
            cell.messageTextView.frame = CGRect(x: 46 + 8, y: 0, width: estimatedFrame.width + 16, height: estimatedFrame.height + 18)
            cell.textBubbleView.frame = CGRect(x: 46, y: 0, width: estimatedFrame.width + 30, height: estimatedFrame.height + 18)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let messageText = manager.messages[indexPath.item].content
        let estimatedFrame = NSString(string: messageText).boundingRect(with: CGSize(width: width * porcent, height: 1000000), options: NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin), attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: messageTextFontSize)], context: nil)
        self.collectionViewLength += estimatedFrame.height + 18 + 10
        return CGSize(width: width, height: estimatedFrame.height + 18)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 8, left: 0, bottom: 28, right: 0)
    }
    
    @objc func handleSend(){
        if(self.manager.connected) {
            self.manager.tchatapi.socket.emit("message", ["type": "chat", "message": inputTextField.text!, "user": self.manager.current!.id])
            self.manager.ajouterMessage(message: Message(content: inputTextField.text!, date: Date(), user: self.manager.current))
            print(self.manager.current!, self.manager.messages.count)
            collectionView?.insertItems(at: [IndexPath(item: self.manager.messages.count - 1, section: 0)])
            print(self.manager.current!, self.manager.messages.count)
            scrollBottom()
            inputTextField.text = ""
        } else {
            let alert = UIAlertController(title: "Erreur", message: "Erreur de l'envoie du message", preferredStyle: .alert)
            alert.defineType(type: .error)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func scrollBottom() {
        self.collectionView?.scrollToItem(at: IndexPath(item: self.manager.messages.count - 1,  section: 0), at: .bottom, animated: true)
        //print(self.collectionViewLength, self.collectionView.frame.height)
        //self.collectionView?.scrollRectToVisible(CGRect(x: self.collectionViewLength, y: 0, width: 1, height: 1), animated: true)
        self.collectionView?.scrollRectToVisible(CGRect(x: 0, y: self.collectionViewLength, width: 1, height: self.collectionViewLength), animated: true)
    }
    
}

class TchatMessageCell: UICollectionViewCell {
    
    public let messageTextView : UITextView = {
        let textView = UITextView()
        textView.backgroundColor = UIColor.clear
        textView.font = UIFont.systemFont(ofSize: messageTextFontSize)
        textView.isEditable = false
        return textView
    }()
    
    public let textBubbleView: UIView = {
       let view = UIView()
        view.backgroundColor = UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.2)
        view.layer.cornerRadius = 15
        view.layer.masksToBounds = true
        return view
    }()
    
    public let profilImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 15
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews(){
        backgroundColor = UIColor.clear
        addSubview(textBubbleView)
        addSubview(messageTextView)
        addSubview(profilImageView)
        addConstraintsWithFormat(format: "H:|-8-[v0(30)]", views: profilImageView)
        addConstraintsWithFormat(format: "V:[v0(30)]-3-|", views: profilImageView)
    }
    
    func setMessageTextView(isSender: Bool) {
        if isSender {
            messageTextView.textColor = UIColor.darkGray
            textBubbleView.backgroundColor = UIColor.init(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.3)
        } else {
            messageTextView.textColor = UIColor.white
            textBubbleView.backgroundColor = UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.2)
        }
    }
}

func setImageProfilFromURL(url: String, imgView: UIImageView) {
    let imageUrl : URL = URL(string: url)!
    DispatchQueue.global().async {
        let imageData:NSData = NSData(contentsOf: imageUrl)!
        
        DispatchQueue.main.async {
            let image = UIImage(data: imageData as Data)
            imgView.image = image
        }
    }
}

extension UIColor {
    public convenience init(hexa: String){
        var r, g, b, a: CGFloat
        r = 1
        g = 1
        b = 1
        a = 1
        if hexa.hasPrefix("#") {
            let start = hexa.index(hexa.startIndex, offsetBy: 1)
            let hexaColor = String(hexa[start...])
            
            if hexaColor.count == 8 {
                let scanner = Scanner(string: hexaColor)
                var hexaNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexaNumber) {
                    r = CGFloat((hexaNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexaNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexaNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat((hexaNumber & 0x000000ff)) / 255
                }
            }
        }
        self.init(red: r, green: g, blue: b, alpha: a)
    }
}
